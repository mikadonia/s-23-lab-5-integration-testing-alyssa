## BVA table

| Parameter        | Equivalence class           |
|------------------|-----------------------------|
| distance         | `<=0`,`>0`                  |
| type             | `budget`,`luxury`,`nonsense`       |
| plan             | `minute`,`fixed_price`, `nonsense` |
| planned_distance | `<=0`,`>0`                  |
| time             | `<=0`,`>0`                  |
| planned_time     | `<=0`,`>0`                  |
| inno_discount    | `yes`,`no`,`nonsense`              |


## Parameters

Budet car price per minute = 15 <br /> 
Luxury car price per minute = 31 <br /> 
Fixed price per km = 13 <br /> 
Allowed deviations in % = 16 <br /> 
Inno discount in % = 9

### Discovered bugs

1. Rides are available with negative distance
2. Rides are available with zero planned distance 
3. Rides are available with zero planned tim

## Decision table

| type    | plan           | distance  | planned_distance | time           | planned_time   | inno_discount | expected        | actual          | passed |
|---------|----------------|-----------|------------------|----------------|----------------|---------------|-----------------|-----------------|--------|
| nonsense| minute         | 1         | 1                | 1              | 1              | yes           | invalid         | invalid         | Yes    |
| budget  | nonsense       | 1         | 1                | 1              | 1              | yes           | invalid         | invalid         | Yes    |
| budget  | minute         | -1        | 1                | 1              | 1              | yes           | invalid         | 13.65           | No     |
| budget  | minute         | 1000001   | 1                | 1              | 1              | yes           | invalid         | 13.65           | Yes    |
| budget  | minute         | 1         | -1               | 1              | 1              | yes           | invalid         | 13.65           | No     |
| budget  | minute         | 1         | 1000001          | 1              | 1              | yes           | invalid         | 13.65           | Yes    |
| budget  | minute         | 1         | 1                | -1             | 1              | yes           | invalid         | invalid         | Yes    |
| budget  | minute         | 1         | 1                | 1000001        | 1              | yes           | invalid         | 13650013.65     | Yes    |
| budget  | minute         | 1         | 1                | 1              | -1             | yes           | invalid         | 13.65           | No     |
| budget  | minute         | 1         | 1                | 1              | 1000001        | yes           | invalid         | 13.65           | Yes    |
| budget  | minute         | 1         | 1                | 1              | 1              | nonsense      | invalid         | invalid         | Yes    |
| budget  | minute         | 0         | 1                | 1              | 1              | yes           | invalid         | 13.65           | No     |
| budget  | minute         | 1000000   | 1                | 1              | 1              | yes           | 13.65           | 13.65           | Yes    |
| budget  | minute         | 1         | 0                | 1              | 1              | yes           | invalid         | 13.65           | No     |
| budget  | minute         | 1         | 1000000          | 1              | 1              | yes           | 13.65           | 13.65           | Yes    |
| budget  | minute         | 1         | 1                | 0              | 1              | yes           | invalid         | 0               | No     |
| budget  | minute         | 1         | 1                | 1000000        | 1              | yes           | 13650000.0      | 13650000        | Yes    |
| budget  | minute         | 1         | 1                | 1              | 0              | yes           | invalid         | 13.65           | No     |
| budget  | minute         | 1         | 1                | 1              | 1000000        | yes           | 13.65           | 13.65           | Yes    |
| budget  | minute         | 10        | 100              | 250            | 500            | yes           | 3412.5          | 3412.5          | Yes    |
| budget  | minute         | 10        | 100              | 250            | 500            | no            | 3750            | 3750            | Yes    |
| budget  | fixed_price    | 10        | 100              | 250            | 500            | yes           | 3412.5          | 3791.6666666666 | No     |
| budget  | fixed_price    | 10        | 100              | 250            | 500            | no            | 3750            | 4166.6666666666 | No     |
| luxury  | minute         | 10        | 100              | 250            | 500            | yes           | 7052.5          | 4936.75         | No     |
| luxury  | minute         | 10        | 100              | 250            | 500            | no            | 7750            | 5425            | No     |
| luxury  | fixed_price    | 10        | 100              | 250            | 500            | yes           | 7052.5          | invalid         | No     |
| luxury  | fixed_price    | 10        | 100              | 250            | 500            | no            | 7750            | invalid         | No     |

